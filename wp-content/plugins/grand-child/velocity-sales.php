<?php

function promotion_func(){

    ?>
    <style>
        tr.salerow {border: 1px solid;}
    </style>
    <div class="wrap" id="grandchild-backend">
            <h2>Retailer Promotions </h2>

    <div id="wpcontent1" class="client_info_wrap">
             <form name="pluginname" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
            <table class="form-table">
                <tr>
                    <th colspan="2"><h4>Settings</h4></th>
                </tr>
                
                
            <tr>
            <td width="150px">
                <label for="clientsecret">Sale information Json <strong>*</strong></label>
            </td>
            <td class="form-group">
                <textarea name="saleinformation" style="width:70%;"><?php echo get_option('saleinformation')?></textarea>

            </td>
        </tr>

        
        <tr>
            <td width="150px">
                <label for="clientsecret">Overwrite Module <strong>*</strong></label>
            </td>
            <td class="form-group">
                <input type="checkbox" name="overright_module" value="1" />Overright Modules(Content Slider, post grid etc.)

            </td>
        </tr>
        <tr>
            <td></td>
            <td class="form-group">
                <button id="syncdata" type="submit" class="button button-primary" >Save</button>
            </td>
        </tr>
                
        <tr>
            <th colspan="2"><h4>Current Active Promotions</h4></th>
        </tr>                
        
        <?php $saleinfo = get_option('promos_json');

        if($saleinfo != ''){
        
        $saleinfo = json_decode($saleinfo, True);        

        foreach($saleinfo as $sale){

        ?>
        <tr class="salerow">
            <td width="150px">
                <span><b><?php echo $sale['name']; ?> - <?php echo $sale['promoCode'];  ?></b></span>
            </td>
            <td width="150px">
                <span><?php echo 'Start Date -: '.$sale['startDate'];  ?> - <?php echo 'End Date -: '.$sale['endDate'];  ?> </span>
                <br>
                <span><?php echo 'Priority -: '.$sale['priority'];  ?> </span>
                <br>
                <?php //print_r($sale['widgets']);
                
                foreach($sale['widgets'] as $widget){
                    ?>

                        <br>
                        <span><?php echo 'Widget Name -: '.$widget['name'];  ?> <?php if($widget['type'] == 'slide'){echo '===>Slide Order -: '.$widget['order']; } ?></span>
                        <br>
                        <span><?php echo 'Type -: '.$widget['type'];  ?> </span>
                        <br>
                        
              <?php  }
                ?>
            </td>
        </tr>     
       
        <?php } }?>   
        </table>    
        </form>
        </div>
        </div>
    <?php
}